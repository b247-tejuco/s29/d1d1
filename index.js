// Comparison Query Operator
// SubSection $gt / $gte Operator lt lte if less than
	
	// allow us find docs dat have field no. values greater than or equal to specific value
// syntax
// db.collectionName.find({ field: {$gt: value} })   or lt 
// db.collectionName.find({ field: {$gte: value} }) or lte 

db.users.find ({ age: {$gt: 50}});


// subsection $ne operator

// find doc that have field number value NOT EQUAL to a specified value
// syntax
//db.users.collectionName.find({fied: {$ne: value}})

db.users.find({age: {$ne: 82}})

// subsection $in operator
// allows us to find document with sepcific match critera one field using dfiff values
// syntax: db.collectionName.find({field: {$in: value}})

db.users.find({lastName: {$in: ["Hawking", "Doe"]}})
db.users.find({courses: {$in: ["HTML", "React"]}})

// section logical query operators
// sub section $or operator
// allows us to find documetns that match a single criteria from multiple provuded search criteria.

// syntax: db.collectionName.find({$or: [{fieldA: valueA},{fieldB: valueB}]})

multiple field value pairs
db.users.find({$or:

	[

		{firstName: "Neil"},
		{age: 21}

	]
})

db.users.find({$or: 
		[
			{firstName: "Neil"},
			{age: {$lt: 30}}
		]
})

//subsection $and operator
//allows us to find documentt matching criteria in a single field

// syntax
// db.collectionName.find({$and:[{fieldA:valueA}, {fieldB:valueB}]})

db.users.find({$and: 

	[
	 {age: {$lt: 66}},

	 {age: {$gt: 64}}
	]
})

// [section] field projection
// retrieving documents are ocmmon operations that we do and by default MongoDB
// queries return the whole document as a respone
// when dealing with complex data structures

db.users.find({firstName: "Jane"}, {firstName: 1, lastName: 1, contact: 1})
db.users.find(
		{
			firstName:"Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			contact:1,
		}
	);

//  like inclusion but exclusion  use 0 isntead of 1 

db.users.find(
		{
			firstName:"Jane"
		},
		{
			firstName: 0,

		}
	);

// subsection suppressing the id field
// allows us to exclude the "_id" field when retrieving documents
// syntax:
// db.collectionName.find({criteria},{_id:0})

db.users.find({firstName:"Jane"},{firstName:1, lastName:1,contact:1, _id:0})



// embedded

db.users.find({firstName:"Jane"},{firstName:1, lastName:1,"contact.phone":1})


// suppress specific field embeded 
// syntax
// db.collectionsNmae.find({field:$regex: 'apttern', $options: '$optionValue'})

db.users.find({firstName: "Jane"}, {"contact.phone": 0})
// regex operator allow find doc tha match a specifi string pattern using expressions
db.users.find({firstName:{$regex:'N'}}) //case sensitive


db.users.find({firstName:{$regex:'j', $options:'$i'}}) //case insensitive


